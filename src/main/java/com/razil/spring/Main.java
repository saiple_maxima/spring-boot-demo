package com.razil.spring;

import com.razil.spring.entity.Student;
import com.razil.spring.repository.StudentRepository;
import com.razil.spring.repository.TeacherRepository;
import com.razil.spring.service.LogService;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Optional;

@SpringBootApplication
@RequiredArgsConstructor
public class Main implements CommandLineRunner {

    private final StudentRepository studentRepository;

    private final TeacherRepository teacherRepository;


    public static void main(String[] args) {
        SpringApplication.run(Main.class, args);
    }

    @Override
    public void run(String... args) throws Exception {

        Optional<Student> student = studentRepository.findById(17L);

        int i = 0;

    }
}
