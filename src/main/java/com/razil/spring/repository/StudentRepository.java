package com.razil.spring.repository;

import com.razil.spring.entity.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Set;

public interface StudentRepository extends JpaRepository<Student, Long> {

    List<Student> findByIdIn(Set<Long> ids);

    @Query(nativeQuery = true, value = "select * from student where name = 'Anton'")
    List<Student> getAntons();
}
