package com.razil.spring.service;

import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class DateService {

    public Date date(){
        return new Date();
    }
}
